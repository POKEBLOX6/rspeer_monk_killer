package org.ianfights;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.local.Health;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import java.awt.*;

@ScriptMeta(
    version = 0.99,
    name = "MonkKiller",
    category = ScriptCategory.COMBAT,
    developer = "ianfights",
    desc = "Kills Zamorak monks and picks up the monk bottoms."
)


public class Main extends Script implements RenderListener {

    StopWatch timer;
    private static int BALLSMADE;
    public static final Area temple = Area.rectangular(2949, 3519, 2929, 3512, 0);
    public static String FoodItem;


    private void bank() {
        if (Bank.isOpen()) {
            Bank.depositInventory();
            if (!Bank.contains(FoodItem)) {}
        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a - > true);
            }
        }
    }


    String status = "";

    public void onStart() {
        FoodItem = null;
        new BattleGUI().setVisible(true);

        timer = StopWatch.start();
    }

    private int getProg() {
        return BALLSMADE;
    }

    Player me = Players.getLocal();
    Area BANK_AREA = Area.rectangular(2943, 3373, 2949, 3366, 0);


    @Override
    public int loop() {

        Npc monk_of_zamorak = Npcs.getNearest(x - > x.getName().equals("Monk of Zamorak") && (x.getTargetIndex() == -1 || x.getTarget().equals(me)) && x.getHealthPercent() > 0);
        Pickable zamorak_monk_bottom = Pickables.getNearest(x - > x.getName().equals("Zamorak monk bottom") && x.distance(me) < 20 && x.distance(temple.getCenter()) < 20);
        Pickable zamorak_monk_top = Pickables.getNearest(x - > x.getName().equals("Zamorak monk bottom") && x.distance(me) < 20 && x.distance(temple.getCenter()) < 20);


        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > Random.nextInt(5, 15))
            Movement.toggleRun(true);
        if (!Inventory.contains(FoodItem) && !BANK_AREA.contains(me)) {
            Movement.walkTo(BANK_AREA.getCenter());
            Log.info("No Food banking.");
            //Log.fine(FoodItem);
        }


        if (BANK_AREA.contains(me) && Bank.isOpen() && Bank.contains(FoodItem)) {
            Log.fine("Getting food");
            Bank.withdraw(FoodItem, 20);

            if (!Bank.contains(FoodItem)) {
                setStopping(true);

            }

        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a - > true);
            }
        }



        if (Inventory.contains(FoodItem) && !Inventory.isFull()) {
            if (me.distance(temple.getCenter()) >= 5) {
                //status = "Walking";
                //Log.fine("Walking");
                Movement.walkTo(temple.getCenter());
            }
            // status = "Fighting";
            if (Health.getPercent() < 30) {
                Inventory.getFirst(FoodItem).interact("Eat");
            }

            if (zamorak_monk_bottom != null) {
                //BALLSMADE++;
                zamorak_monk_bottom.interact("Take");
            } else {
                if (me.getTargetIndex() == -1 && !me.isMoving()) {


                    monk_of_zamorak.interact("Attack");
                    Time.sleepUntil(me::isAnimating, Random.nextInt(4000, 5000));



                }
            }


            if (zamorak_monk_top != null) {
                BALLSMADE++;
                zamorak_monk_top.interact("Take");
            }


        }

        if (Inventory.isFull()) {
            //status = "Banking";
            //Log.info("Walking to bank..");

            if (!BANK_AREA.contains(me)) {
                Movement.walkTo(BANK_AREA.getCenter());
            }
            if (BANK_AREA.contains(me)) {


                Log.info("Attempting to bank inventory..");
                bank();

            }


        }


        return Random.nextInt(500, 800);
    }

    public void notify(RenderEvent m) {
        Graphics g = m.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 35;
        int x = 10;
        g2.setColor(Color.GREEN);
        g2.drawString("ianfight's Monk Killer", x, y);
        g2.drawString("Runtime: " + timer.toElapsedString(), x, y += 20);
        g2.drawString("Items Collected: " + getProg(), x, y += 20);
        if (Inventory.isFull()) {
            g2.drawString("Status: " + "Banking", x, y += 20);
        }
        if (!temple.contains(me)) {
            g2.drawString("Status: " + "Walking to Temple", x, y += 20);
        }
        if (temple.contains(me)) {
            g2.drawString("Status: " + "Fighting", x, y += 20);
        }
    }
}
